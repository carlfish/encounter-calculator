module PureCss where

import Halogen.HTML.Core (ClassName(..))

table :: ClassName
table = ClassName "pure-table"

form :: ClassName
form = ClassName "pure-form"