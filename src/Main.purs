module Main where

import Prelude

import Effect (Effect)
import Halogen.Aff as HA
import Halogen.VDom.Driver (runUI)

import Calculator as C

main :: Effect Unit
main = HA.runHalogenAff do
  body <- HA.awaitBody
  runUI C.component unit body