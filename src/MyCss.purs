module MyCss where

import Halogen.HTML.Core (ClassName(..))

results :: ClassName
results = ClassName "results"

resultTable :: ClassName
resultTable = ClassName "resulttable"