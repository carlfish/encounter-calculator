module Calculator where

import Prelude

import Data.Array          (group, findIndex, index, replicate)
import Data.Array.NonEmpty (head)
import Data.Enum           (upFromIncluding)
import Data.Foldable       (length, foldl)
import Data.Int            (toNumber, fromString)
import Data.Maybe          (Maybe(..), fromMaybe)
import Data.Tuple          (Tuple(..))

import Halogen                 as H
import Halogen.HTML            as HH
import Halogen.HTML.Events     as HE
import Halogen.HTML.Properties as HP

import Encounter                       as E
import MyCss                           as MC
import PureCss                         as PC
import Range     (start, end, limitTo) as E

data Query a
  = Recalculate (String -> State -> State) String a

type State = 
  { partySize :: Int
  , partyLevel :: E.Level
  , includeCount :: Int
  , includeCR :: E.CR
  , minCR :: E.CR
  , maxCR :: E.CR
  , maxSize :: Int
  , junkFilter :: Int
  } 

levels :: Array E.Level
levels = upFromIncluding E.L1

levelToInt :: E.Level -> Int
levelToInt level = fromMaybe 1 $ ((+) 1) <$> findIndex ((==) level) levels

intToLevel :: Int -> Maybe E.Level
intToLevel i = index levels (i - 1)

crs :: Array E.CR
crs = upFromIncluding E.CR0

crToInt :: E.CR -> Int
crToInt cr = fromMaybe 1 $ ((+) 1) <$> findIndex ((==) cr) crs

intToCr :: Int -> Maybe E.CR
intToCr i = index crs (i - 1)

component :: forall m. H.Component HH.HTML Query Unit Void m
component =
  H.component
    { initialState: const initialState
    , render
    , eval
    , receiver: const Nothing
    }
  where
    initialState :: State
    initialState = 
      { partySize: 5
      , partyLevel: E.L4
      , includeCount: 0
      , includeCR: E.CR1_4
      , minCR: E.CR1_4
      , maxCR: E.CR8
      , maxSize: 12
      , junkFilter: 16
      }

    render :: State -> H.ComponentHTML Query
    render state =
      let
        party = [ Tuple state.partySize state.partyLevel ]
        seed = replicate state.includeCount state.includeCR
        result = E.filterEncounters (E.removeFiller state.junkFilter) $ E.encounters seed party (E.Range state.minCR state.maxCR) state.maxSize
        rng range = "(" <> (show $ E.start range) <> "xp - " <> (show $ E.end range) <> "xp)" 
        selected p = if p then [ HP.selected true ] else [ HP.selected false] 
      in
        HH.div_ 
          [ HH.h2_ [ HH.text "Options:"]
          , HH.form [ HP.class_ PC.form ]
            [ HH.fieldset_ 
              [ HH.legend_ [ HH.text "Party Composition"]
              , HH.label [ HP.for "partySize" ] [ HH.text "# " ]
              , HH.input 
                [ HP.id_ "partySize"
                , HP.type_ HP.InputNumber
                , HP.min (toNumber 1)
                , HP.max (toNumber 20)
                , HP.value (show $ state.partySize)
                , HE.onValueChange (HE.input (Recalculate (\s -> _{ partySize = intField 1 20 s })))
                ]
              , HH.label [ HP.for "partyLevel" ] [ HH.text " Level: " ]
              , HH.input 
                [ HP.id_ "partyLevel"
                , HP.type_ HP.InputNumber
                , HP.min (toNumber 1)
                , HP.max (toNumber 20)
                , HP.value (show (levelToInt state.partyLevel))
                , HE.onValueChange (HE.input (Recalculate (\s -> _{ partyLevel = intStringTo E.L1 intToLevel s })))
                ]
              ]
            , HH.fieldset_ 
              [ HH.legend_ [ HH.text "Enemy Composition"]
              , HH.label [ HP.for "minCR" ] [ HH.text " Min CR: " ]
              , HH.select 
                [ HP.id_ "minCR"
                , HE.onValueChange (HE.input (Recalculate (\s -> _{ minCR = (intStringTo E.CR1_8 intToCr s) })))
                ]
                ((\cr -> 
                  HH.option ([ HP.value (show $ crToInt cr) ] <> selected (cr == state.minCR)) [ HH.text (show cr) ]
                ) <$> crs)
              , HH.label [ HP.for "maxCR" ] [ HH.text " Max CR: " ]
              , HH.select 
                [ HP.id_ "maxCR"
                , HE.onValueChange (HE.input (Recalculate (\s -> _{ maxCR = (intStringTo E.CR1_8 intToCr s) })))
                ]
                ((\cr -> 
                  HH.option ([ HP.value (show $ crToInt cr) ] <> selected (cr == state.maxCR)) [ HH.text (show cr) ]
                ) <$> crs)
              , HH.label [ HP.for "maxSize" ] [ HH.text " Max Encounter Size: " ]
              , HH.input 
                [ HP.id_ "maxSize"
                , HP.type_ HP.InputNumber
                , HP.min (toNumber 1)
                , HP.max (toNumber 30)
                , HP.value (show state.maxSize)
                , HE.onValueChange (HE.input (Recalculate (\s -> _{ maxSize = intField 1 30 s })))
                ]
              ]
            , HH.fieldset_ 
              [ HH.legend_ [ HH.text "Prepopulate"]
              , HH.label [ HP.for "includeCount" ] [ HH.text " Include at least: " ]
              , HH.input 
                [ HP.id_ "includeCount"
                , HP.type_ HP.InputNumber
                , HP.min (toNumber 1)
                , HP.max (toNumber 20)
                , HP.value (show $ state.includeCount)
                , HE.onValueChange (HE.input (Recalculate (\s -> _{ includeCount = intField 0 20 s })))
                ]              
              , HH.label [ HP.for "includeCR" ] [ HH.text " CR: " ]
              , HH.select 
                [ HP.id_ "includeCR"
                , HE.onValueChange (HE.input (Recalculate (\s -> _{ includeCR = (intStringTo E.CR1_8 intToCr s) })))
                ]
                ((\cr -> 
                  HH.option ([ HP.value (show $ crToInt cr) ] <> selected (cr == state.includeCR)) [ HH.text (show cr) ]
                ) <$> crs)
              ]
            , HH.fieldset_ 
              [ HH.legend_ [ HH.text "Junk Filter"]
              , HH.input 
                [ HP.id_ "junkFilter"
                , HP.type_ HP.InputRange
                , HP.min (toNumber 0)
                , HP.max (toNumber 20)
                , HP.value (show state.junkFilter)
                , HE.onValueChange (HE.input (Recalculate (\s -> _{ junkFilter = intField 0 20 s })))
                ]
              ]
            ]
          , HH.h2_ [ HH.text "Encounters:" ]
          , HH.div [ HP.class_ MC.results ]
            [ (resultsTable party ("Hard " <> rng (E.difficultyRange E.Hard party))  (E.hard result))
            , (resultsTable party ("Medium " <> rng (E.difficultyRange E.Medium party))  (E.medium result))
            , (resultsTable party ("Easy " <> rng (E.difficultyRange E.Easy party))  (E.easy result))
            ]
          ]
        

    resultsTable :: E.Party -> String -> Array (Array E.CR) -> H.ComponentHTML Query
    resultsTable party head es =
      HH.table [ HP.classes [ PC.table, MC.resultTable ] ]
        [ HH.thead_ 
          [ HH.tr_
            [ HH.th [ HP.colSpan 4 ] 
              [ HH.text head ] 
            ]
          , HH.tr_
            [ HH.th_ [ HH.text "Adjusted XP" ]
            , HH.th_ [ HH.text "Actual XP" ]
            , HH.th_ [ HH.text "Size" ]
            , HH.th_ [ HH.text "Monsters" ]
            ]
          ]
        , HH.tbody_ (encRows party es)
        ]

    encRows :: E.Party -> Array (Array E.CR) -> Array (H.ComponentHTML Query)
    encRows p = (<$>) (encRow p)

    encRow :: E.Party -> Array E.CR -> H.ComponentHTML Query
    encRow p es =
      let
        squashedCR a = 
          if (length a == 1) then (show (head a))
          else (show (length a :: Int)) <> "x" <> (show (head a))
        crlist arr = foldl (\s es' -> s <> (squashedCR es') <> " ") "" (group arr)
      in
        HH.tr_
            [ HH.td_ [ HH.text (show $ E.adjustedXP p es) ]
            , HH.td_ [ HH.text (show $ E.rawXP es ) ]
            , HH.td_ [ HH.text (show $ (length es :: Int)) ]
            , HH.td_ [ HH.text (crlist es) ]
            ]

    eval :: Query ~> H.ComponentDSL State Query Void m
    eval = case _ of
      Recalculate f s next -> do
        _ <- H.modify (f s)
        pure next
        
stringTo :: forall a b. Ord a => b -> (a -> a) -> (String -> Maybe a) -> (a -> Maybe b) -> String -> b
stringTo default sanitizer parser decoder input =
  fromMaybe default (decoder =<< (sanitizer <$> parser input))

intStringTo :: forall a. a -> (Int -> Maybe a) -> String -> a
intStringTo default decoder =
  stringTo default identity fromString decoder

stringToLimit ::  forall a b. Ord a => b -> E.Range a -> (String -> Maybe a) -> (a -> Maybe b) -> String -> b
stringToLimit default range parser decoder input = 
  stringTo default (E.limitTo range) parser decoder input

intField :: Int -> Int -> String -> Int
intField min max = stringToLimit min (E.Range min max) fromString Just