module Encounter
  ( module Exports
  , encounters
  , difficultyRange
  , adjustedXP
  , partyThresholds
  , rawXP
  , easy
  , medium
  , hard
  , removeFiller
  , filterEncounters
  ) where

import Prelude

import Data.Array          (snoc, group, groupBy, reverse, sort, sortWith, filter)
import Data.Array.NonEmpty (NonEmptyArray, toArray, head)
import Data.Enum           (pred)
import Data.Foldable       (class Foldable, foldMap, foldr, length, sum)
import Data.Maybe          (Maybe(..), maybe)
import Data.Tuple          (Tuple(..))

import Encounter.Types     (CR, Difficulty(..), Encounters(..), Party, PartySize, Thresholds(..), XP(..))
import Encounter.Tables    (adjustXP, encounterSize, partySize, thresholds, xp)
import Range               (Range(..), end, start, to)

-- More Limited imports for reexport

import Encounter.Types     (CR(..), Level(..), Difficulty(..), Party, Thresholds(..), XP(..), Encounters(..)) as Exports
import Range               (Range(..)) as Exports

-- "filler" is defined as an encounter where some CR has been added to make up numbers without
-- contributing sufficiently to the encounter. Filter strength is 0-20 (0 is no filtering, 
-- higher than 20 is just treated as 20)
removeFiller :: Int -> Array CR -> Boolean
removeFiller strength xs =
  let
    grouped = group xs
    totalXP = rawXP xs
    adj = if      strength > 20 then 1000 
          else if strength < 0  then 0
          else                  2000 - (50 * strength)
    isNotFiller as = (totalXP * (XP 1000)) `div` (rawXP as) <= XP ((length grouped) * adj)
  in 
    if (strength <= 0) then true
    else foldr (\as b -> b && (isNotFiller as)) true grouped

-- Difficulty Helpers

difficulty :: Thresholds -> XP -> Difficulty
difficulty (Thresholds t) x = 
  if      (x < t.easy)   then Trivial
  else if (x < t.medium) then Easy
  else if (x < t.hard)   then Medium
  else if (x < t.deadly) then Hard
  else                        Deadly

difficultyRange :: Difficulty -> Party -> Range XP
difficultyRange d p = difficultyRange' d (partyThresholds p)

difficultyRange' :: Difficulty -> Thresholds -> Range XP
difficultyRange' d (Thresholds ts) =
  let
    xp1 = XP 1
  in case d of
    Trivial -> (XP 0)    `to` (ts.easy - xp1)
    Easy    -> ts.easy   `to` (ts.medium - xp1)
    Medium  -> ts.medium `to` (ts.hard - xp1)
    Hard    -> ts.hard   `to` (ts.deadly - xp1)
    Deadly  -> ts.deadly `to` (ts.deadly * (XP 10))

-- Encounter Building

encounters :: Array CR -> Party -> Range CR -> Int ->  Encounters
encounters seed party crRange maxSize =
  let
    ts = partyThresholds party
    maxXP = end $ difficultyRange' Hard ts
    ps = partySize party
    encs = calculateEncounters seed ps crRange maxXP maxSize
    grouped =
      groupBy (\a b -> (difficulty ts (adjustedXP' ps a) == difficulty ts (adjustedXP' ps b))) $
        reverse $
          sortWith (adjustedXP' ps) encs
    toEnc nea = case (difficulty ts (adjustedXP' ps (head nea))) of
      Trivial -> mempty
      Easy    -> toEasy (toArray nea)
      Medium  -> toMedium (toArray nea)
      Hard    -> toHard (toArray nea)
      Deadly  -> mempty
  in
    foldMap toEnc grouped

groupByThresholds :: Thresholds -> PartySize -> Array (Array CR) -> Array (NonEmptyArray (Array CR))
groupByThresholds ts ps ar =
  groupBy (\a b -> (difficulty ts (adjustedXP' ps a) == difficulty ts (adjustedXP' ps b))) $
    reverse $
        sortWith (adjustedXP' ps) ar

partyThresholds :: Party -> Thresholds
partyThresholds p =
  let
    times i (Thresholds t) = Thresholds 
      { easy:   t.easy   * (XP i)
      , medium: t.medium * (XP i)
      , hard:   t.hard   * (XP i)
      , deadly: t.deadly * (XP i)
      }
    toThresholds (Tuple c l) = c `times` (thresholds l)
  in
    foldMap toThresholds p

-- Knapsack problem-ish.
-- Find the largest CR monster within our allowed range that we can add to the encounter without breaking our budget.
--   - If we can't find one, the encounter is full
--   - If we can find one:
--     - recurse to find encounters including another monster of that CR
--     - recurse to find encounters _not_ including another monster of that CR (by shrinking the allowed range).
calculateEncounters :: Array CR -> PartySize -> Range CR -> XP -> Int -> Array (Array CR)
calculateEncounters seed ps crRange maxXP maxSize = 
  let
    seedFits = adjustedXP' ps seed <= maxXP
    encs = calculateEncounters' seed ps crRange maxXP maxSize
  in
    if seedFits then
      (reverse <<< sort) <$> encs
    else
      []

calculateEncounters' :: Array CR -> PartySize -> Range CR -> XP -> Int -> Array (Array CR)
calculateEncounters' acc ps crRange maxXP maxSize = 
  let
    narrow cr = Range (start crRange) cr
    recurseWith newAcc newRange =
      calculateEncounters' newAcc ps newRange maxXP maxSize
    recurseWithMatch cr = 
      recurseWith (snoc acc cr) (narrow cr)
    recurseWithoutMatch =
      maybe [ acc ] (\cr -> recurseWith acc (narrow cr))
    recurse cr =
      recurseWithMatch cr <> recurseWithoutMatch (pred cr)
  in
    if (length acc >= maxSize)
      then [ acc ]
      else maybe [ acc ] recurse $ findLargestFit ps crRange acc maxXP

findLargestFit :: PartySize -> Range CR -> Array CR -> XP -> Maybe CR
findLargestFit ps (Range low high) crs maxXP =
  let
    fits cr = (adjustedXP' ps (snoc crs cr)) <= maxXP
    tryNext cr =
        if      cr < low then Nothing
        else if fits cr  then Just cr
        else             tryNext =<< (pred cr)
  in
    tryNext high


-- XP Calculation

rawXP :: forall a. Functor a => Foldable a => a CR -> XP
rawXP = sum <<< map xp

adjustedXP :: forall a. Functor a => Foldable a => Party -> a CR -> XP
adjustedXP = adjustedXP' <<< partySize

adjustedXP' :: forall a. Functor a => Foldable a => PartySize -> a CR -> XP
adjustedXP' ps crs =
  let
    eSize = encounterSize ps (length crs)
  in
    adjustXP eSize (rawXP crs)

-- Encounter Helpers

toEasy :: Array (Array CR) -> Encounters
toEasy e = Encounters { easy: e, medium: [], hard: [] }

toMedium :: Array (Array CR) -> Encounters
toMedium e = Encounters { easy: [], medium: e, hard: [] }

toHard :: Array (Array CR) -> Encounters
toHard e = Encounters { easy: [], medium: [], hard: e }

easy :: Encounters -> Array (Array CR)
easy (Encounters e) = e.easy

medium :: Encounters -> Array (Array CR)
medium (Encounters e) = e.medium

hard :: Encounters -> Array (Array CR)
hard (Encounters e) = e.hard

filterEncounters :: (Array CR -> Boolean) -> Encounters -> Encounters
filterEncounters f (Encounters e) = Encounters
 e { easy = filter f e.easy, medium = filter f e.medium, hard = filter f e.hard }
