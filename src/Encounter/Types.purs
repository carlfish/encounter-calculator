module Encounter.Types
  ( Thresholds(..)
  , Difficulty(..)
  , Encounters(..)
  , Party(..)
  , XP(..)
  , Level(..)
  , CR(..)
  , PartySize(..)
  , EncounterSize(..)
  ) where

import Prelude

import Data.Enum             (class Enum)
import Data.Foldable         (foldr)
import Data.Generic.Rep      (class Generic)
import Data.Generic.Rep.Enum (genericPred, genericSucc)
import Data.Tuple            (Tuple)

data Thresholds = Thresholds
  { easy   :: XP
  , medium :: XP
  , hard   :: XP
  , deadly :: XP
  }

instance monoidThresholds :: Monoid Thresholds where
  mempty = Thresholds { easy: XP 0, medium: XP 0, hard: XP 0, deadly: XP 0}

instance semigroupThresholds :: Semigroup Thresholds where
  append (Thresholds a) (Thresholds b) = Thresholds
    { easy: a.easy + b.easy
    , medium: a.medium + b.medium
    , hard: a.hard + b.hard
    , deadly: a.deadly + b.deadly
    }

data Difficulty
 = Trivial
 | Easy
 | Medium
 | Hard
 | Deadly

derive instance eqDifficulty  :: Eq Difficulty
derive instance ordDifficulty :: Ord Difficulty

data Encounters = Encounters
  { easy :: Array (Array CR)
  , medium :: Array (Array CR)
  , hard :: Array (Array CR)
  }

instance encountersSemigroup :: Semigroup Encounters where
  append (Encounters a) (Encounters b) = Encounters
    { easy: a.easy <> b.easy
    , medium: a.medium <> b.medium
    , hard: a.hard <> b.hard
    }

instance encountersMonoid :: Monoid Encounters where
  mempty = Encounters { easy: [], medium: [], hard: [] }

instance encountersShow :: Show Encounters where
  show (Encounters e) =
    let
      showEncs arr = foldr (\es s -> s <> "\n\t" <> (show es)) "" arr
    in
      "Hard:" <> (showEncs e.hard) <> "\nMedium:" <> (showEncs e.medium) <> "\nEasy:" <> (showEncs e.easy)

type Party = Array (Tuple Int Level)

newtype XP = XP Int

derive newtype instance eqXp :: Eq XP
derive newtype instance ordXp :: Ord XP
derive newtype instance semiringXp :: Semiring XP
derive newtype instance ringXp :: Ring XP
derive newtype instance commutativeRingXp :: CommutativeRing XP
derive newtype instance euclideanRingXp :: EuclideanRing XP
derive newtype instance showXp :: Show XP

data Level
  = L1
  | L2
  | L3
  | L4
  | L5
  | L6
  | L7
  | L8
  | L9
  | L10
  | L11
  | L12
  | L13
  | L14
  | L15
  | L16
  | L17
  | L18
  | L19
  | L20

derive instance genLevel  :: Generic Level _
derive instance ordLevel  :: Ord Level
derive instance eqLevel   :: Eq Level
instance enumLevel :: Enum Level where
  succ = genericSucc
  pred = genericPred

data CR
  = CR0
  | CR1_8
  | CR1_4
  | CR1_2
  | CR1
  | CR2
  | CR3
  | CR4
  | CR5
  | CR6
  | CR7
  | CR8
  | CR9
  | CR10
  | CR11
  | CR12
  | CR13
  | CR14
  | CR15
  | CR16
  | CR17
  | CR18
  | CR19
  | CR20
  | CR21
  | CR22
  | CR23
  | CR24
  | CR25
  | CR26
  | CR27
  | CR28
  | CR29
  | CR30

derive instance genCR  :: Generic CR _
derive instance ordCR  :: Ord CR
derive instance eqCR   :: Eq CR
instance enumCR :: Enum CR where
  succ = genericSucc
  pred = genericPred


instance showCR :: Show CR where
  show = case _ of
    CR0   -> "CR0"
    CR1_8 -> "CR1/8"
    CR1_4 -> "CR1/4"
    CR1_2 -> "CR1/2"
    CR1   -> "CR1"
    CR2   -> "CR2"
    CR3   -> "CR3"
    CR4   -> "CR4"
    CR5   -> "CR5"
    CR6   -> "CR6"
    CR7   -> "CR7"
    CR8   -> "CR8"
    CR9   -> "CR9"
    CR10  -> "CR10"
    CR11  -> "CR11"
    CR12  -> "CR12"
    CR13  -> "CR13"
    CR14  -> "CR14"
    CR15  -> "CR15"
    CR16  -> "CR16"
    CR17  -> "CR17"
    CR18  -> "CR18"
    CR19  -> "CR19"
    CR20  -> "CR20"
    CR21  -> "CR21"
    CR22  -> "CR22"
    CR23  -> "CR23"
    CR24  -> "CR24"
    CR25  -> "CR25"
    CR26  -> "CR26"
    CR27  -> "CR27"
    CR28  -> "CR28"
    CR29  -> "CR29"
    CR30  -> "CR30"

data PartySize
  = PSmall
  | PMedium
  | PLarge

data EncounterSize
  = ETiny
  | EXSmall
  | ESmall
  | EMedium
  | ELarge
  | EXLarge
  | EXXLarge

derive instance genEncounterSize  :: Generic EncounterSize _
derive instance eqEncounterSize  :: Eq EncounterSize
derive instance ordEncounterSize  :: Ord EncounterSize
instance enumEncounterSize :: Enum EncounterSize where
  succ = genericSucc
  pred = genericPred