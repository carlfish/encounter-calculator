module Encounter.Tables
  ( partySize
  , encounterSize
  , adjustXP
  , thresholds
  , xp
  ) where
  
import Prelude       ((<), (*), div, (<<<), map, ($)) 

import Data.Enum     (succ, pred)
import Data.Foldable (sum)
import Data.Maybe    (fromMaybe)
import Data.Tuple    (fst)

import Encounter.Types

partySize :: Party -> PartySize
partySize =
  let 
    partySize' i = 
      if      i < 3 then PSmall
      else if i < 6 then PMedium
      else               PLarge
  in
    partySize' <<< sum <<< map fst

encounterSize :: PartySize -> Int -> EncounterSize
encounterSize pSize monsters =
  let
    es = 
      if      monsters < 1  then ETiny
      else if monsters < 2  then EXSmall
      else if monsters < 3  then ESmall
      else if monsters < 7  then EMedium
      else if monsters < 11 then ELarge
      else if monsters < 15 then EXLarge
      else                       EXXLarge
  in case pSize of
    PSmall  -> fromMaybe es (succ es) -- go up one modifier if possible
    PMedium ->           es
    PLarge  -> fromMaybe es (pred es) -- go down one modifier if possible

adjustXP :: EncounterSize -> XP -> XP
adjustXP es (XP i) = 
  XP $ case es of
    ETiny    -> i `div` 2
    EXSmall  -> i
    ESmall   -> (i * 3) `div` 2
    EMedium  -> i * 2
    ELarge   -> (i * 5) `div` 2
    EXLarge  -> i * 3
    EXXLarge -> i * 4

thresholds :: Level -> Thresholds
thresholds = 
  let 
    t e m h d = Thresholds { easy: XP e, medium: XP m, hard: XP h, deadly: XP d }
  in case _ of
    L1  -> t   25   50   75   100
    L2  -> t   50  100  150   200
    L3  -> t   75  150  225   400
    L4  -> t  125  250  375   500
    L5  -> t  250  500  750  1100
    L6  -> t  300  600  900  1400
    L7  -> t  350  750 1100  1700
    L8  -> t  450  900 1400  2100
    L9  -> t  550 1100 1600  2400
    L10 -> t  600 1200 1900  2800
    L11 -> t  800 1600 2400  3600
    L12 -> t 1000 2000 3000  4500
    L13 -> t 1100 2200 3400  5100
    L14 -> t 1250 2500 3800  5700
    L15 -> t 1400 2800 4300  6400
    L16 -> t 1600 3200 4800  7200
    L17 -> t 2000 3900 5900  8800
    L18 -> t 2100 4200 6300  9500
    L19 -> t 2400 4900 7300 10900
    L20 -> t 2800 5700 8500 12700

xp :: CR -> XP
xp = XP <<< case _ of
  CR0   ->     10 
  CR1_8 ->     25 
  CR1_4 ->     50 
  CR1_2 ->    100 
  CR1   ->    200 
  CR2   ->    450 
  CR3   ->    700 
  CR4   ->   1100 
  CR5   ->   1800 
  CR6   ->   2300 
  CR7   ->   2900 
  CR8   ->   3900 
  CR9   ->   5000 
  CR10  ->   5900 
  CR11  ->   7200 
  CR12  ->   8400 
  CR13  ->  10000 
  CR14  ->  11500 
  CR15  ->  13000 
  CR16  ->  15000 
  CR17  ->  18000 
  CR18  ->  20000 
  CR19  ->  22000 
  CR20  ->  25000 
  CR21  ->  33000 
  CR22  ->  41000 
  CR23  ->  50000 
  CR24  ->  62000 
  CR25  ->  75000 
  CR26  ->  90000 
  CR27  -> 105000 
  CR28  -> 120000 
  CR29  -> 135000 
  CR30  -> 155000 