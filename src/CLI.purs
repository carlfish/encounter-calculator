module CLI where

import Prelude (($), (<>), (==), show)
import Data.Foldable (foldl, length)
import Data.Array (group)
import Data.Array.NonEmpty (head)
import Encounter (CR, Difficulty(..), Encounters(..), Party, Range, encounters, adjustedXP, difficultyRange, partyThresholds)
import Range (start, end)

prettyPrint' :: Party -> Array CR -> String
prettyPrint' p arr = 
  let
    stem = "(" <> (show $ adjustedXP p arr) <> "xp) "
    squashedCR a = 
      if (length a == 1) then (show (head a))
      else (show (length a :: Int)) <> "x" <> (show (head a))
  in
    foldl (\s es -> s <> (squashedCR es) <> " ") stem (group arr)

prettyPrint :: Party -> Encounters -> String
prettyPrint p (Encounters e) =
    let
        ts = partyThresholds p
        rng range = "(" <> (show $ start range) <> "xp - " <> (show $ end range) <> "xp)" 
        ppEncs arr = foldl (\s es -> s <> "\n\t" <> (prettyPrint' p es)) "" arr
    in
         "Hard: " <> (rng $ difficultyRange Hard p)
      <> (ppEncs e.hard) 
      <> "\nMedium: " <> (rng $ difficultyRange Medium p)
      <> (ppEncs e.medium) 
      <> "\nEasy: "  <> (rng $ difficultyRange Easy p)
      <> (ppEncs e.easy)

showEncounters :: Party -> Range CR -> Int -> String
showEncounters p crr maxSize = prettyPrint p $ encounters [] p crr maxSize