module Range
  ( Range(..)
  , to
  , start
  , end
  , limitTo
  ) where

import Prelude (class Ord, (<), (>))

data Range a = Range a a

to :: forall a. a -> a -> Range a
to a b = Range a b

start :: forall a. Range a -> a     
start (Range a _) = a

end :: forall a. Range a -> a
end (Range _ a) = a

limitTo :: forall a. Ord a => Range a -> a -> a
limitTo range a = 
  if       a > (end range)   then end range
  else if  a < (start range) then start range
  else                            a